#+OPTIONS: toc:nil
#+OPTIONS: num:0
#+OPTIONS: \n:t
#+OPTIONS: author:nil
#+OPTIONS: date:nil
#+OPTIONS: title:nil

#+LATEX_COMPILER: xelatex
#+LATEX_HEADER: \usepackage{ctex}

#+LATEX_HEADER: \usepackage{geometry}
#+LATEX_HEADER: \geometry{a4paper, textwidth=6.5in, textheight=10in, marginparsep=7pt, marginparwidth=.6in}


* 简历
:PROPERTIES:
:ID:       06CBB5E3-17CF-4A8B-AB80-FB6106CF9A46
:END:

of [[id:DE73D78C-DE75-40AC-963B-0BC83BC50D46][documents]]


** 个人信息
| 姓名     | : | 徐付成          |
| 性别     | : | 男              |
| 生日     | : | 1983/8          |
| 电话     | : | (86)18621180023 |
| 电子邮箱 | : | shaqxu@163.com  |
| 国籍     | : | 中国            |


** 教育
| /                 | <            | <        |
| 时间              | 大学         | 专业     |
| 09/2002 - 06/2006 | 南京财经大学 | 经济     |

*** 在职
| /       | <                    | <                                                    |
| 学院    | 课程类别             | 课程                                                 |
| mit ocw | 数学（纯数）         | 18.01, 18.02, 18.03, 18.06, 18.703, 18.S097, 18.100B |
|         | 数学（应用）         | 18.062, 18.S190, 18.S191, 18.404                     |
|         | 电气工程与计算机科学 | 6.001, 6.004, 6.006, 6.034, 6.828                    |



** 工作
| 01/2017-今      | 三之谓信息技术有限公司             |
| 06/2014-12/2016 | 维信智荟互联网金融信息服务有限公司 |
|                 | 高级开发经理                       |
| 08/2007-08/2013 | 花旗软件技术有限公司               |
|                 | 开发, 高级开发经理                 |
| 05/2007-8/2007  | 众恒软件有限公司                   |
|                 | 开发                               |


** 个人简介
10年以上银行和网上金融IT系统开发经验，包括开发、团队领导、技术架构师、高级开发经理。
4年人工智能/机器学习设计、开发和应用经验。

技术:
全栈web开发经验。基础架构、安全架构、应用程序设计、应用程序开发、IT自动化。
人工智能相关算法优化与实现，mlp/cnn/rnn应用。
高性能应用开发。

交付应用:
高性能短链服务系统, 基于ML的信用分系统、花旗在线银行系统(门户网站)、花旗在线信用卡系统(门户网站)、花旗信用卡奖励系统、VCredit在线系统(核心、Web、移动应用、BMS)……


** 项目经验

*** P1
| 01/2017 - 今 | 公司:三之谓信息技术有限公司 |
|              | 高级开发                    |


主要工作业绩:
从硬件到软件的IT基础设施设置
人工智能基础设施实现:梯度下降、牛顿法、拟牛顿法,线性回归,逻辑回归,正规化的回归,反向传播,Forwardpropagation,神经网络、多级分类,对角线偏差与方差(Underfitting &过度拟合分析),精度和召回分析、支持向量机、高斯内核,k - means算法,非监督学习、集群、主成分分析、降维、数据压缩、数据可视化、从压缩表示，异常检测，基于内容的推荐，协同过滤重建
基于ML的信用分系统。本系统不基于规则引擎，不需要手动维护规则。特点:不依赖于专业经验的积累去维护信用分卡片，学习现有客户的状态和行为信息，模型自动校准
高性能短链服务系统


主要技术范围:
rust, Julia, Knet, cnn, rnn, mlp, pca, StatsBase, Makie/ plot, datafrframes, FFMPEG.jl, Images, ImageMagick, VideoIO, symy, Mux
Clojure, CLJS, Incanter, Imagez, Vectorz-clj, Three.js
Ubuntu, Mount, Muuntaja, Hiccup, Dommy, Oauth-clj, Prone, Ring, Secretary, hikarip, Migratus, Buddy, Postal, Timbre, Tufte, Datomic, Postgresql，Reagent(React)


*** P2
| 06/2014 - 12/2016 | 公司:维信金融科技 |
|                   | 部门:在线商务     |
|                   | 角色:高级开发经理 |

维信是一家以个人无抵押贷款、p2p为主营业务的香港上市公司。

主要职责:
VMoney(VCredit子公司)整体IT管理
与业务部门合作，制定产品策略
保证IT部门为公司业务提供快速、优质的支持
新技术调查和编码

主要成就:
成立IT部门(30余人)，包括:UI设计团队、开发团队、移动团队、测试团队、基础架构团队。
交付VMoney在线系统，核心功能包括:VMoney在线、后台管理系统、iOS App、Android App、第三方/银行独立/本地银行存款和管理。
VMoney Online首次完成VCredit的端到端在线商业闭环
开展Scrum & Devops。响应速度快:2小时内完成紧急bug修复，2周内完成新需求从设计到生产
质量控制:90%以上的集成测试都是通过测试自动化来实现的，最小化了副作用，为Scrum和Devops奠定了基础

主要技术范围:
Spring(Spring Boot, Spring Security, Spring Data, Spring Integration)， JTA(bitronix)， OData(Apache Olingo)， Jboss Infinispan, OAuth, Rest(Jersey2, Cxf)， thyymleaf, Angular, NodeJS, Ubuntu, Jboss Wildfly, Nginx, MySQL, Scrum, DevOps, IT/基础自动化(Ansible, Go.cd)


*** P3
| 12/2012 - 08/2013 | 公司:花旗软件技术有限公司 |
|                   | 部门:电子商务             |
|                   | 项目:亚太网银一体化       |
|                   | 角色:开发经理             |


花旗软件技术有限公司是花旗集团旗下的一家软件公司。主要业务是为花旗银行提供技术相关的解决方案。
电子业务部主要负责花旗银行的互联网(在线)系统和移动应用。

主要职责和成绩:
花旗银行在线信用卡系统的整体技术管理。
制定详细的设计、开发、测试、交付计划以满足业务需求。
遵循SDLC流程，保证进度。
识别技术相关风险，采取预防措施，确保所有风险在控制之下。
用scrum流程代替瀑布流程，对业务需求变化做出更快的反应。
核心功能编码。
在13个国家推出统一代码基础。整合了13个不同的代码库，并通过业务规则配置实现特定国家的需求。删除了重复的代码，结果相同的功能部署到多个国家，只需配置工作，也减少了80%以上的维护工作
为花旗在线信用卡系统提供开发和维护支持

主要技术范围:
JavaEE, Struts, Spring webflow, Unix, Websphere, DB2, MQ, Webservice, SPA，测试自动化(Selenium)


*** P4
| 1/2010 -12/2012 | 公司:花旗软件技术有限公司 |
|                 | 部门:电子商务             |
|                 | 项目:AP区域BAU支持        |
|                 | 角色:高级开发             |

职责:
为新加坡、香港、马来西亚、菲律宾提供7 * 24小时在线信用卡系统生产支持
基础设施和核心功能编码
对新技术的应用进行概念验证


*** P5
| 08/2007-12/2009 | 公司:花旗软件技术有限公司       |
|                 | 部门:电子商务                   |
|                 | 项目:香港网上卡申请系统及AP彩虹 |
|                 | 角色:开发                       |


职责:开发、测试、文档化

主要技术范围:
Java金融平台(基于struts的花旗内部框架)、jsp、javascript、jquery、IBM MQ、DB2、websphere、CI


*** P6
| 2007.04 - 2007.08 | 公司:众恒软件有限公司    |
|                   | 项目:上海闵行政府GIS系统 |
|                   | 角色:开发                |

众恒是一家专业为政府提供GIS软件的软件公司

职责:开发、测试

主要技术范围:
servlet, jsp, javascript, oracle, websphere


** 附 技术栈
技术(掌握级别)

*** 操作系统
gentoo(代码级别: 网络组件), xv6(代码级别: 全部)，ubuntu(结构级别)

*** 编辑器
emacs(结构与二次开发)

*** 开发语言
高性能：rust, c, c++
网络应用: clojure, java, javascript
编缉器：elisp
人工智能：julia

*** 库与框架
**** 高性能
clap, tokio, tower, hyper, tonic, axum, tracing, opentelemetry, config

**** 网络应用
***** 后端
nginx, mysql, oracle, mq, websphere
spring(spring boot, spring security, spring data, spring integration, spring webflow)
ring.clj, mount.clj
***** 前端
angular, react, reagent.clj, hiccup.clj, dommy.clj

**** DevOps
ansible, jenkins, go.cd, selenium

**** 人工智能
flux, knet, opencv, ffmpeg, augmentor.jl, videoio.jl, images.jl, makie.jl
incanter.clj, imagez.clj
